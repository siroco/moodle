#!/bin/bash

set -e

# moodle config file /app/moodle/config.php
config() {
	if [ -n $MYSQL_DATABASE ]; then sed -i "s:dbname.*:dbname = '$MYSQL_DATABASE';:" /app/moodle/config.php; fi
	if [ -n $MYSQL_HOST ]; then sed -i "s:dbhost.*:dbhost = '$MYSQL_HOST';:" /app/moodle/config.php; fi
	if [ -n $MYSQL_USER ]; then sed -i "s:dbuser.*:dbuser = '$MYSQL_USER';:" /app/moodle/config.php; fi
	if [ -n $MYSQL_PASSWWORD ]; then sed -i "s:dbpass.*:dbpass = '$MYSQL_PASSWORD';:" /app/moodle/config.php; fi
	if [ -n $WWW_ROOT ]; then sed -i "s#wwwroot.*#wwwroot = '$WWW_ROOT';#" /app/moodle/config.php; fi
	if [ -n $DATA_ROOT ]; then sed -i "s#dataroot.*#dataroot = '$DATA_ROOT';#" /app/moodle/config.php; fi
}

create_data_dir() {
	if [ -n $DATA_ROOT ]; then mkdir -p /app/moodledata; fi
}

config
create_data_dir

echo "Iniciando Moodle"

exec "$@"
