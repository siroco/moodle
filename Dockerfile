FROM debian:bullseye-slim as builder

ENV VERSION "latest-311"
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \ 
		apt-get install -qy curl

WORKDIR /app

RUN set -e; curl -L "https://download.moodle.org/stable311/moodle-latest-311.tgz" > moodle-latest-311.tgz && \
		curl -L "https://download.moodle.org/stable311/moodle-latest-311.tgz.md5" > moodle-latest-311.tgz.md5 && \
		md5sum --check "moodle-latest-311.tgz.md5"
RUN tar xvzf moodle-latest-311.tgz

ADD config/config.php moodle/

RUN mkdir moodledata && \
	chmod 777 moodledata

RUN rm -rf *.tgz

RUN apt-get -qy install \
    php-fpm php-mysql php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip php-imagick && \
    apt-get clean

COPY config/moodle.conf /etc/php/7.4/fpm/pool.d/www.conf
COPY config/php.ini /etc/php/7.4/fpm/php.ini

RUN mkdir -p /run/php && \
	chmod o+w /run/php

COPY docker-entrypoint.sh /app/docker-entrypoint.sh
RUN chmod +x /app/docker-entrypoint.sh

EXPOSE 9000

WORKDIR /app/moodle

VOLUME ["/app/moddle","/app/moddledata"]

ENTRYPOINT ["/app/docker-entrypoint.sh"]

CMD ["/usr/sbin/php-fpm7.4","--nodaemonize"]]
